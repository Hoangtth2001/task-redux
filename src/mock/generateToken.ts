import sha256 from "crypto-js/sha256";
import hmacSHA512 from "crypto-js/hmac-sha512";
import Base64 from "crypto-js/enc-base64";
import { User } from "./users";
function generateToken(user: User): string {
  // Kết hợp id và userName để tạo chuỗi cần mã hóa
  const dataToHash = user.id + user.userName;

  // Sử dụng thuật toán SHA256 để mã hóa chuỗi
  const hash = sha256(dataToHash);
  const privateKey = "hoangth";
  // Lấy giá trị hash dưới dạng hex
  const token = Base64.stringify(hmacSHA512(hash, privateKey));

  return token;
}

export default generateToken;
