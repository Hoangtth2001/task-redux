import MockAdapter from "axios-mock-adapter";
import { User, accessUsers } from "./users";

// Mô phỏng đăng nhập thành công
export const onPostlogin = (mock: MockAdapter, postUser: Omit<User, "id">) => {
  mock.onPost("/user/login", postUser).reply(() => {
    const findData = accessUsers.find(
      (user) =>
        user.password === postUser.password &&
        user.userName === postUser.userName
    );

    if (findData) {
      const { password, ...finalUser } = findData;
      return [200, finalUser];
    } else {
      return [401, { message: "user not found" }];
    }
  });
};
// export const;
// // Mô phỏng đăng nhập thất bại
// const loginFail = mock.onPost("/login").reply(401, {
//   error: "Invalid credentials",
// });

// export { loginFail };
