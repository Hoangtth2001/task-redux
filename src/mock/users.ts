import generateToken from "./generateToken";

export interface User {
  id: string;
  userName: string;
  password: string;
}

export interface AccessUser extends User {
  token: string|null;
}
export const usersData: User[] = [
  {
    id: "1",
    userName: "admin",
    password: "12345678",
  },
  {
    id: "2",
    userName: "hoang",
    password: "12345678",
  },
  {
    id: "2",
    userName: "hoang2k1",
    password: "12345678",
  },
  {
    id: "3",
    userName: "taik3",
    password: "12345678",
  },
];

// Tạo mảng AccessUser từ mảng usersData với token được sinh ra bởi generateToken
export const accessUsers: AccessUser[] = usersData.map((user) => ({
  ...user,
  token: generateToken(user),
}));
