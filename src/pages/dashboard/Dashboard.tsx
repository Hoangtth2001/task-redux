import { useSelector } from "react-redux";
import { RootState } from "../../store";
import { Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
const btnstyle = { margin: "8px 0", width: "25%" };

const Dashboard = () => {
  const navigate = useNavigate();
  const isLogging = useSelector(
    (state: RootState) => state.userLogin.isLogging
  );
  useEffect(() => {
    const getToken = localStorage.getItem("access_token");
    if (getToken) {
      navigate("/dashboard");
    } else {
      navigate("/login");
    }
  }, [isLogging, navigate]);
  const handleLogout = () => {
    localStorage.removeItem("access_token");
    navigate("/login");
  };
  return (
    <div>
      <h2>dashBoard</h2>
      <Button
        type="submit"
        color="primary"
        variant="contained"
        style={btnstyle}
        onClick={handleLogout}
      >
        Log Out
      </Button>
    </div>
  );
};

export default Dashboard;
