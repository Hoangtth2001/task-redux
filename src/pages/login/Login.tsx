import React, { useEffect, useState } from "react";
import {
  Avatar,
  Button,
  Checkbox,
  FormControlLabel,
  Grid,
  Paper,
  TextField,
  Typography,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import PunchClockIcon from "@mui/icons-material/PunchClock";
import axios from "axios";
import { onPostlogin } from "../../mock/mockApi";
import MockAdapter from "axios-mock-adapter";

import { RootState, useAppDispatch } from "../../store";
import { userLogin } from "./login.slice";
import { User } from "../../mock/users";
import { useSelector } from "react-redux";
const mock = new MockAdapter(axios);

const paperStyle = {
  padding: 20,
  height: "70vh",
  width: 280,
  margin: "20px auto",
};
const inputLogin: User = {
  userName: "",
  password: "",
  id: "",
};
const avatarStyle = { backgroundColor: "#1bbd7e" };
const btnstyle = { margin: "8px 0" };
const Login = () => {
  const dispatch = useAppDispatch();
  const [formData, setformData] = useState<User>(inputLogin);
  const navigate = useNavigate();
  const error = useSelector((state: RootState) => state.userLogin.error);

  const loginfail = useSelector(
    (state: RootState) => state.userLogin.logginfail
  );

  const handleLogin = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    dispatch(
      userLogin({
        userName: formData.userName,
        password: formData.password,
      })
    )
      .unwrap()
      .then((data) => {
        navigate("/dashboard");
        localStorage.setItem("access_token", JSON.stringify(data.token));
      })
      .catch((error) => {
        console.log(error);
      });
  };
  useEffect(() => {
    const onLogin = async () => {
      onPostlogin(mock, {
        userName: formData.userName,
        password: formData.password,
      });
    };
    onLogin(); // create a mock post api
    const getToken = localStorage.getItem("access_token");

    if (getToken) {
      navigate("/dashboard");
    } else {
      navigate("/login");
    }
  }, [formData, navigate]);

  return (
    <Grid>
      <form onSubmit={handleLogin}>
        <Paper elevation={10} style={paperStyle}>
          <Grid>
            <Avatar style={avatarStyle}>
              <PunchClockIcon />
            </Avatar>
            <h2>Sign In</h2>
          </Grid>
          <TextField
            label="userName"
            placeholder="Enter userName"
            type="text"
            fullWidth
            value={formData.userName}
            onChange={(e) =>
              setformData((prev) => ({ ...prev, userName: e.target.value }))
            }
            required
          />
          <TextField
            label="Password"
            placeholder="Enter password"
            type="password"
            fullWidth
            value={formData.password}
            onChange={(e) =>
              setformData((prev) => ({ ...prev, password: e.target.value }))
            }
            required
          />
          <FormControlLabel
            control={<Checkbox name="checkedB" color="primary" />}
            label="Remember me"
          />
          {loginfail && <div>{error}</div>}

          <Button
            // onClick={handleNaigate}
            type="submit"
            color="primary"
            variant="contained"
            style={btnstyle}
            fullWidth
          >
            Sign In
          </Button>

          <Typography></Typography>
        </Paper>
      </form>
    </Grid>
  );
};

export default Login;
