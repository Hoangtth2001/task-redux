import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { AccessUser, User } from "../../mock/users";
import axios from "axios";

export const userLogin = createAsyncThunk(
  "auth/login",
  async (body: Omit<User, "id">, thunkApi) => {
    const res = await axios.post<AccessUser>("/user/login", body, {
      signal: thunkApi.signal,
    });
    return res.data;
  }
);

interface State {
  isLogging: boolean;
  loginSuccess: boolean;
  logginfail: boolean;
  currentUser?: AccessUser | null;
  error: string;
}

export const initialState: State = {
  isLogging: false,
  loginSuccess: false,
  logginfail: true,
  currentUser: null,
  error: "",
};

// interface AuthState{

// }
const authSlice = createSlice({
  name: "auth",
  initialState: initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(userLogin.fulfilled, (state, action) => {
        state.loginSuccess = true;
        state.isLogging = true;
        state.logginfail = false;
        state.currentUser = action.payload;
      })
      .addCase(userLogin.pending, (state, action) => {
        state.currentUser = null;
        state.isLogging = false;
        state.loginSuccess = true;
      })
      .addCase(userLogin.rejected, (state, action) => {
        state.logginfail = true;
        state.loginSuccess = false;
        state.isLogging = false;
        state.currentUser = null;
        state.error = "userName or password is incorrect";
      });
  },
});

const authReducer = authSlice.reducer;
export default authReducer;
